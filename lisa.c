/*===============================================
Author             : Bhupendra Naphade
File             : Lisa_Scrambling.c
Copyright         :
Description        : Program to Implement LISA with
                  data scrambling and LBC technique
===============================================*/
#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include "cr_section_macros.h"

#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "math.h"
#include "lisa.h"
#include "lbc.h"


#define TICKRATE_HZ1 10
#define rate 500000
#define delay_val 0.5

char text[100];
int ord;
int genrator_typ,lisa_typ;
int opcounter = 0;
int oplength = 0;
bool int_recieved_flag=0;//flag to indicate interupt
void SetGPIO(uint8_t port, uint32_t pin)
{
    if (port == 0)
    {
        LPC_GPIO0->FIOSET |= (1 << pin);
    }
    else if (port == 1)
    {
        LPC_GPIO1->FIOSET |= (1 << pin);
    }
    else if (port == 2)
    {
        LPC_GPIO2->FIOSET |= (1 << pin);
    }
    else
    {
        printf("Not a valid port!\n");
    }
    return;
}

void ClrGPIO(uint8_t port, uint32_t pin)
{
    if (port == 0)
    {
        LPC_GPIO0->FIOCLR |= (1 << pin);
    }
    else if (port == 1)
    {
        LPC_GPIO1->FIOCLR |= (1 << pin);
    }
    else if (port == 2)
    {
        LPC_GPIO2->FIOCLR |= (1 << pin);
    }
    else
    {
        printf("Not a valid port!\n");
    }
    return;
}

void GPIOInit()
{
    //LPC_PINCON->PINMODE0 = 0x00000010;
    LPC_GPIO2->FIODIR &= ~(1<<rx);
    LPC_GPIO2->FIODIR |= (1<<tx);
    LPC_GPIO2->FIODIR |= (1<<12);
    ClrGPIO(2,tx);
}
//int j;

void delayMs(uint32_t delayInMs)
{
    LPC_TIM0->TCR = 0x02;        /* reset timer */
    LPC_TIM0->PR  = 0x00;        /* set prescaler to zero */
    LPC_TIM0->MR0 = delayInMs*delay_val* (9000000 / 1000-1);
    LPC_TIM0->IR  = 0xff;        /* reset all interrrupts */
    LPC_TIM0->MCR = 0x04;        /* stop timer on match */
    LPC_TIM0->TCR = 0x01;        /* start timer */

    /* wait until delay time has elapsed */
    while (LPC_TIM0->TCR & 0x01);

  return;
}

void getPayloadASCII(int max, char *p)
{
    int i = 0,cnt=0,temp=0,j=0;
    int ASCIIPayload[max/8];
    ASCIIPayload[0]='\0';

    while(i<max)
    {   temp=0;
        for(cnt=0;cnt<8;cnt++)
            {
                temp += (p[i] - '0')*pow(2,(7-cnt));
                ++i;
            }
        ASCIIPayload[j]=temp;
        ++j;
        printf("%c",temp);

    }

}

int getindex(int *a, char *str)
{
    int indexarr[4];
    int cnt;
    int index=0;
    char x;

    for(cnt=0;cnt<4;cnt++)
    {
        x = str[*a];
        indexarr[cnt]= (x - '0');
        *a +=1;
        index += indexarr[cnt]*pow(2,(3-cnt));
    }
    return index;
}

void receive()
{
    while(1)
    {
        if(int_recieved_flag)
        {
        //	SetGPIO(2,12);
        int i = 0 ;
        int j = 0 ;
        int x,y;
        unsigned char *payload;
        bool descramble [4096];
        int cntr=0,str_len=0;
        int del3=0,del7=0;
        ClrGPIO(2,tx);
        while(i<4096)
        {
            if(LPC_GPIO2->FIOPIN & (1<<rx))
            {
                SetGPIO(2,12);
                buff[i]=1;
                cntr++;
            }
            else
            {
                ClrGPIO(2,12);
                buff[i]=0;
                cntr=0;
            }
            if(cntr==15)
            {
                break;
            }

            delayMs(1);
            i++;
        }
//        printf("\n");
//        for(j=0;j<i;j++)
//        {
//            printf("%d",buff[j]);
//        }
      //  printf("\nvalue of i :%d\n",i);
        str_len=i-14;
      //  printf("\nstring length : %d\n",str_len);
        if(1){
            y=identify(buff);
            printf("Sync index:: %d\n",y);
            fflush(stdout);
            x=payload_start(y,buff);
            printf("Payload Start:: %d\n",x*8+y);
            str_len=str_len-(x*8+y);
           // printf("\nstring length : %d\n",str_len);

            if((str_len%12)!=0)
            {
                str_len = str_len+(12-(str_len%12));
            }
            printf("\nstring length : %d\n",str_len);
            fflush(stdout);
            int data_length,scramble_order,generator_typ;
            data_length = bin_to_dec(buff,x*8+y);
            printf("\ndata_length:%d",data_length);
            scramble_order = bin_to_dec(buff,x*8+y+8);
            printf("\nscramble_order: %d",scramble_order);
            generator_typ  = bin_to_dec(buff,x*8+y+16);
            printf("\ngenerator_typ :%d\n",generator_typ);
            // int *buff_lbc;
            bool DecodedData[data_length*8];
            if(generator_typ==1)
            {
            	lbc_decode(generator_typ,x,y,data_length,buff,DecodedData);
            }
            else
            {
            	lbc_decode2(generator_typ,x,y,data_length,buff,DecodedData);
            }
                        //Descramble
            descrambler(scramble_order,data_length,DecodedData,descramble);
            //  bool del3,del7;

            //               printf("\nDescramble data : ");
            //               for(i=0;i<str_len+16;i++)
            //                   {printf("%d",descramble[i]);
            //
            //                   }
            printf("\nReceived Data : ");

            payload = bintochar(descramble,0,data_length*8);
        }
        break;
        }
    }
}
void transmiter(char *text)
{
ClrGPIO(2,tx);
    //char text[]= {"The world is reflection of your mind. Change your thoughts and the world will ***"};
    bool BinText[strlen(text)*8];//12
    int i =0;
    bool SyncField[] =
       {1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,1,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,1,1,0,1,0,0,1,0,0,
        1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,1,
        1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,1,0,0,1,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0,
        1,0,1,0,1,1,1,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,0,1,0,0,1,0,1,0,0,1,1,
        0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,0,1,0,1,0,1,1,1,0,1,0,1,1,0,0,0,
        0,1,0,1,1,0,0,1,0,1,0,1,1,0,1,0,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0,0,0,1,0,1,1,1,0,1,
        0,1,0,1,1,1,1,0,0,1,0,1,1,1,1,1};

    bool end_bits[] ={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};

    int temp[8];
    int t = 0, count = 0,j,k;
    for(j=0;j<strlen(text);j++)
        {
            t=text[j];
            for(i=0;i<8;i++)
            {
                temp[i]=t%2;
                t /=2;
            }
            for(k=7;k>=0;k--)
            {
                BinText[count]=temp[k];//+'0';
                count++;
            }
        }
      //  strcat(output,SyncField);
//        printf("Data to be Transmitted: %s\n",text);
//        printf("BinText : %s\n",BinText);
       //scrambler
        int length_data = 0;
        length_data =strlen(text);

       // char length_str_char[8];
        bool length_str_bin[8], scramble_ord_bin[8],gen_typ[8];

         dec_to_bin(length_data,length_str_bin);
         dec_to_bin(ord,scramble_ord_bin);
         dec_to_bin(genrator_typ,gen_typ);
     //  strcat(output,length_str_char);
      //  printf("\n%s\n",output);
        int N,codedtextsize;
        bool scramble [4096];
        if(genrator_typ == 1)
        	codedtextsize = strlen(text)*12;
        else if (genrator_typ == 2)
        	codedtextsize = strlen(text)*14;

        bool CodedText[codedtextsize];
      //  bool ip[4096];

        N = sizeof(BinText);
        scrambler(N,ord,BinText,scramble);
        N =strlen(text);
        if(genrator_typ == 1)
        {
        	lbc_encode(N,scramble,CodedText);
        }
        else if(genrator_typ == 2)
        {
        	lbc_encode2(N,scramble,CodedText);
        }



            i=0;

//         for(i=0;i<strlen(text)*12;i++)
//        {
//            BinText[i]=CodedText[i]+'0';
//        }
        //end scrambler*/
       // strcat(output,BinText);
      //  printf("oplength : %d",oplength);
      //  printf("size bintext : %d",sizeof(BinText));
		if(lisa_typ == 1)
		{
					transmit(SyncField,127,256);
		}
		else{
			transmit(SyncField,0,256);
		}
            transmit(length_str_bin,0,8);
            transmit(scramble_ord_bin,0,8);
            transmit(gen_typ,0,8);
            transmit(CodedText,0,codedtextsize);
            transmit(end_bits,0,16);
        ClrGPIO(2,tx);

//        printf("\nBinary Coded Text    : ");
//                for(i=0;i<(strlen(text)*12);i++)
//                    printf("%d",CodedText[i]);
//                printf("\n\n");

}
void GPIO_INT_config()
{
	LPC_GPIOINT->IO2IntEnR |= (1<<rx);
	NVIC_EnableIRQ(EINT3_IRQn);
	LPC_GPIOINT->IO2IntClr |= (1<<rx);
    return;
}
void EINT3_IRQHandler (void)
{
	int_recieved_flag = 1;
	LPC_GPIOINT->IO2IntEnR &= ~(1<<rx);
	NVIC_DisableIRQ(EINT3_IRQn);
	LPC_GPIO2->FIODIR &= ~(1<<rx);
	//GPIOInit();
}
int main(void) {

        volatile static int i = 0 ;

        GPIOInit();
        ClrGPIO(2,tx);


          //  genrator_typ =1;
#if 1
        printf("Select from below menu\n"
                "1. Transmit Data\n"
                "2. Receive Data\n"
                "Your Selection:");
        fflush(stdin);
        scanf("%d",&i);
        fflush(stdin);
        switch(i)
        {
        case 1:
        	printf("Enter Input string : ");
     	   	fgets(text,1000,stdin);
     	if ((strlen(text)>0) && (text[strlen (text) - 1] == '\n'))
     		text[strlen (text) - 1] = '\0';
		printf("Enter LISA type(1 : 128 bits or 2 :256 bits): ");
		   scanf("%d",&lisa_typ);
			printf("Enter order for scrambling(5,7,9): ");
			scanf("%d",&ord);
			//ord=7;
			printf("Enter genrator type(1 or 2): ");
			   scanf("%d",&genrator_typ);
			  // printf("Transmitting.... ");
						transmiter(text);
							 break;
        case 2:
            GPIO_INT_config();
                receive();

                 break;
        default: printf("Enter Correct Selection\n");
                 break;
        }
#endif

    while(1) {

        }
    return 0 ;
}
