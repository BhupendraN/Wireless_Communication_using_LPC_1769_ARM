Wireless Communication using LPC 1769 ARM Cortex-M3 module by adhering to IEEE 802.11b

- Established Wireless communication between two RF- modules interfaced to LPC1769 by using 802.11 standard (Linear Invariant Synchronization Algorithm) for wireless communication.
- Integrated Scrambler, Descrambler, Modulation, Linear Block Coding and CCK coding into the design.

File Details
- lisa.c : Implementation of transmit and recieve algorithm
- lisa.h : Implementation of LISA coding and decoding, scrambling and de scrambling
- LBC.h  : Implementation of linear block coding