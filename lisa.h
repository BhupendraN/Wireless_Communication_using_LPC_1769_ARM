/*===============================================
Author 			: Bhupendra Naphade
File 			: Lisa.h
Copyright 		:
Description		: Program to Implement LISA with
			      data scrambling technique
===============================================*/
#ifndef LISA_H_
#define LISA_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "lbc.h"

#define sync_overhead_bits 256
#define payload_overhead_bits 256
#define sync_overhead_bytes 32
#define payload_size_bytes 32
#define buffer_size_bytes 64
#define buffer_size 1024
#define tx 3
#define rx 11
char bintocharbyte(bool *,int,int);
bool buff[4096];
//bool buff_lbc[2000];
bool findindex(bool * buffer,int current_index,int next_index)
{
    if(bintocharbyte(buffer,next_index,next_index+8)-bintocharbyte(buffer,current_index,current_index+8)==1)
        return true;
    else
        return false;


}



int identify (bool * buffer)
{
    int i;
    int index1=0, index2=0;
    for(i=0;i<(buffer_size/2);i++)
    {
        if((((buffer[i])^0)+((buffer[i+1])^1)+((buffer[i+2])^0)+((buffer[i+3]^1)))==4)
        {
            index1=i;
            if((((buffer[i+8])^0)+((buffer[i+9])^1)+((buffer[i+10])^0)+((buffer[i+11]^1)))==4)
            {
                if(findindex(buffer,index1,i+8))
                {
                    return i;
                }
            }
        }
        if((((buffer[i])^1)+((buffer[i+1])^0)+((buffer[i+2])^1)+((buffer[i+3]^0)))==4)
        {
            index2=i;

            if((((buffer[i+8])^1)+((buffer[i+9])^0)+((buffer[i+10])^1)+((buffer[i+11]^0)))==4)
            {

                if(findindex(buffer,index2,i+8))
                    {
                        return i;
                    }
            }
        }
    }
    return 0;
}


void chartobin(char x,bool *a,int j)
{
    int i,k;
    for(i=0,k=7;i<8,k>=0;i++,k--)
    {
        a[i+j]=x&(1<<k);
    }
    k=0;
}

char bintocharbyte(bool *a,int index,int end)
{
    unsigned char character=NULL;
    int i,k=7;
    for (i = index; i < end; i++) {

        character|=a[i]<<k;
        k--;
    }
    return character;
}

unsigned char * bintochar(bool *a,int index,int end)
{
	int j=0,temp=0,cnt=0;
	unsigned char *ASCIIPayload[32];
	    ASCIIPayload[0]='\0';
	    while(index<end)
	    {   temp=0;
	        for(cnt=0;cnt<8;cnt++)
	            {
	                temp += a[index]*pow(2,(7-cnt));
	                ++index;
	            }
	        ASCIIPayload[j]=temp;
	        ++j;
	        printf("%c",temp);
	      }
	    return ASCIIPayload;

}

int payload_start(int sync_info,bool *buffer)
{
    unsigned char x= bintocharbyte(buffer,sync_info,sync_info+8);
    switch(x)
    {
        case 0xA0:
            return 32;
        case 0xA1:
            return 31;
        case 0xA2:
            return 30;
        case 0xA3:
            return 29;
        case 0xA4:
            return 28;
        case 0xA5:
            return 27;
        case 0xA6:
            return 26;
        case 0xA7:
            return 25;
        case 0xA8:
            return 24;
        case 0xA9:
            return 23;
        case 0xAA:
            return 22;
        case 0xAB:
            return 21;
        case 0xAC:
            return 20;
        case 0xAD:
            return 19;
        case 0xAE:
            return 18;
        case 0xAF:
            return 17;
        case 0x50:
            return 16;
        case 0x51:
            return 15;
        case 0x52:
            return 14;
        case 0x53:
            return 13;
        case 0x54:
            return 12;
        case 0x55:
            return 11;
        case 0x56:
            return 10;
        case 0x57:
            return 9;
        case 0x58:
            return 8;
        case 0x59:
            return 7;
        case 0x5A:
            return 6;
        case 0x5B:
            return 5;
        case 0x5C:
            return 4;
        case 0x5D:
            return 3;
        case 0x5E:
            return 2;
        case 0x5F:
            return 1;
    }

    return 0;
}

void dec_to_bin(int decimalNumber, bool *binaryNumber)
{
	int quotient;
	int i=1;
    quotient = decimalNumber;
	   for(i=7;i>=0;i--)
	    {
	         binaryNumber[i]= quotient % 2;
	         quotient = quotient / 2;
	    }
}
int bin_to_dec(bool *a, int index)
{
	int temp=0,cnt=0;
	for(cnt=0;cnt<8;cnt++)
	{
		temp += a[index]*pow(2,(7-cnt));
		++index;
	}
	return temp;
}
void transmit(bool *data,int start,int end)
{
	int i;
for(i=start;i<end;i++)
    {
        if (data[i]==1)
        {
			SetGPIO(2,tx);
            SetGPIO(2,12);
            //printf("1");
        }
        else if (data[i]==0)
        {
            ClrGPIO(2,tx);
            ClrGPIO(2,12);
          //  printf("0");
        }
        delayMs(1);
    }
   // printf("\n");
}

void scrambler(int N,int ord,bool *BinText,bool *scramble)
{
	int i,ord_by_2;
	 bool del3,del7;
	ord_by_2 = (ord/2)+1;
	for(i=0;i<N;i++)
			{
			    if(i>=ord_by_2)
			    {
			        del3=scramble[i-ord_by_2];
			    }
			    else
			    {
			        del3 = 0;
			    }
			    if(i>=ord)
			    {
			        del7=scramble[i-ord];
			    }
			    else
			    {
			        del7 = 0;
			    }

			    scramble[i] =BinText[i]^del3^del7;
			}
}

void descrambler(int scramble_order, int data_length,bool *DecodedData, bool *descramble)
{		int i,j=0;
bool del3,del7;
	 int srcam_by_2 =(scramble_order/2)+1;
	            for(i=0;i<data_length*8;i++)//for(i=(x*8)+y;i<(x*8)+y+256;i++)
	            {
	                if(j>=srcam_by_2)
	                {
	                    del3=DecodedData[i-srcam_by_2];
	                }
	                else
	                {
	                    del3 = 0;
	                }
	                if(j>=scramble_order)
	                {
	                    del7=DecodedData[i-scramble_order];
	                }
	                else
	                {
	                    del7 = 0;
	                }

	                descramble[j] =(del3^del7)^DecodedData[i];
	                j++;
	            }
}
void lbc_encode(int N,bool *scramble,bool *CodedText)
{
	bool G[8][12] ={G1,G2,G3,G4,G5,G6,G7,G8};
       // bool CodedText[N*12];//strlen(text) to make dynamic string length
        int i,j,counter=0;
        int BinCounter=0;

        while(counter < N*12)
        {
            for (i=0; i<12; i++)
            {
                CodedText[counter] = 0;
                for (j=0;j<8;j++)
                {
                    CodedText[counter]=CodedText[counter]^(scramble[BinCounter+j]*G[j][i]);
                    //printf("%d ",CodedText[counter]);
                }
                counter++;
                //printf("\n");
            }
            BinCounter += 8;
        }
//	    printf("\nGenerator Matrix:\n");
//	    for (i=0;i<8;i++)
//	    {
//	        for(j=0;j<12;j++)
//	            printf("%d ",G[i][j]);
//	        printf("\n");
//	    }

}
void lbc_encode2(int N,bool *scramble,bool *CodedText)
{
	bool Gen2[8][14] ={G21,G22,G23,G24,G25,G26,G27,G28};
       // bool CodedText[N*12];//strlen(text) to make dynamic string length
        int i,j,counter=0;
        int BinCounter=0;

        while(counter < N*14)
        {
            for (i=0; i<14; i++)
            {
                CodedText[counter] = 0;
                for (j=0;j<8;j++)
                {
                    CodedText[counter]=CodedText[counter]^(scramble[BinCounter+j]*Gen2[j][i]);
                    //printf("%d ",CodedText[counter]);
                }
                counter++;
                //printf("\n");
            }
            BinCounter += 8;
        }
//	    printf("\nGenerator Matrix:\n");
//	    for (i=0;i<8;i++)
//	    {
//	        for(j=0;j<12;j++)
//	            printf("%d ",G[i][j]);
//	        printf("\n");
//	    }

}
void lbc_decode(int generator_typ,int x,int y,int data_length,bool *buff,bool *DecodedData)
{
	bool H[12][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
	            bool G[8][12] ={G1,G2,G3,G4,G5,G6,G7,G8};
	            int i=0,j=0;
	            //Generate Parity Check matrix using generator matrix
	            for (i=0;i<8;i++)
	            {
	                for(j=0;j<4;j++)
	                {
	                    H[i][j] = G[i][j+8];
	                }
	            }
	            for (j=0;j<4;j++)
	            {
	                H[8+j][j]=1;
	            }

	            int InpSize;

	            InpSize=data_length*12;// length of data on gen_typ 12 for type 1
	            //                          printf("\n\nInput Array : ");
	            //                for(i=(x*8)+y;i<(x*8)+y+384;i++)
	            //                       printf("%d",buff[i]);
	            //                   printf("\n\n");

	            // printf("\nInpSize: %d \n",InpSize);


	            //Generate Syndrome Matrix
	            bool Syndrome[InpSize/3];
	            bool *SyndromeDataPointer = Syndrome;
	            bool *ReceivedDataPointer = buff+((x*8)+y+24);
	            int counter=0;
	            int InpCounter=(x*8)+y+24;
	            while(counter < InpSize/3)
	            {
	                for (i=0; i<4; i++)
	                {
	                    Syndrome[counter] = 0;
	                    for (j=0;j<12;j++)
	                    {
	                        Syndrome[counter]=Syndrome[counter]^(buff[InpCounter+j]*H[j][i]);
	                        //printf("%d ",CodedText[counter]);
	                    }
	                    counter++;
	                    //printf("\n");
	                }
	                InpCounter += 12;
	            }

/*
	            i=0;
	            printf("Syndrome matrix:\n");
	            while(i<InpSize/3)
	            {
	                for(j=0;j<4;j++)
	                {
	                    printf("%d ",Syndrome[i]);
	                    i++;
	                }
	                printf("\n");
	            }
*/


	            i=0;j=0;
	            int k=0,l=0;
	            int Distance;
	            x=0;



	            while(i<InpSize/3)
	            {
	                // printf("x = %d\n",x);
	                x = (SyndromeDataPointer[i]^0)+(SyndromeDataPointer[i+1]^0)+(SyndromeDataPointer[i+2]^0)+(SyndromeDataPointer[i+3]^0);
	                //  printf("x = %d\n",x);
	                ///printf("i = %d\n",i);
	                //printf("j = %d\n",j);
	                //printf("l = %d\n",l);

	                k=0;
	                if (x!=0)
	                {
	                    DecodedData[l]=ReceivedDataPointer[j];
	                    DecodedData[l+1]=ReceivedDataPointer[j+1];
	                    DecodedData[l+2]=ReceivedDataPointer[j+2];
	                    DecodedData[l+3]=ReceivedDataPointer[j+3];
	                    DecodedData[l+4]=ReceivedDataPointer[j+4];
	                    DecodedData[l+5]=ReceivedDataPointer[j+5];
	                    DecodedData[l+6]=ReceivedDataPointer[j+6];
	                    DecodedData[l+7]=ReceivedDataPointer[j+7];

	                    while(k<1128) //1128 is length of coded ascii array
	                    {
	                        Distance= (CodedASCIITable[k]^ReceivedDataPointer[j])+
	                                (CodedASCIITable[k+1]^ReceivedDataPointer[j+1])+
	                                (CodedASCIITable[k+2]^ReceivedDataPointer[j+2])+
	                                (CodedASCIITable[k+3]^ReceivedDataPointer[j+3])+
	                                (CodedASCIITable[k+4]^ReceivedDataPointer[j+4])+
	                                (CodedASCIITable[k+5]^ReceivedDataPointer[j+5])+
	                                (CodedASCIITable[k+6]^ReceivedDataPointer[j+6])+
	                                (CodedASCIITable[k+7]^ReceivedDataPointer[j+7])+
	                                (CodedASCIITable[k+8]^ReceivedDataPointer[j+8])+
	                                (CodedASCIITable[k+9]^ReceivedDataPointer[j+9])+
	                                (CodedASCIITable[k+10]^ReceivedDataPointer[j+10])+
	                                (CodedASCIITable[k+11]^ReceivedDataPointer[j+11]);
	                        //printf("Distance: %d\n",Distance);
	                        if (Distance == 1)
	                        {
	                            DecodedData[l]=CodedASCIITable[k];
	                            DecodedData[l+1]=CodedASCIITable[k+1];
	                            DecodedData[l+2]=CodedASCIITable[k+2];
	                            DecodedData[l+3]=CodedASCIITable[k+3];
	                            DecodedData[l+4]=CodedASCIITable[k+4];
	                            DecodedData[l+5]=CodedASCIITable[k+5];
	                            DecodedData[l+6]=CodedASCIITable[k+6];
	                            DecodedData[l+7]=CodedASCIITable[k+7];
	                            break;
	                        }
	                        //printf("k = %d\n",k);
	                        k+=12; //coded ascii table counter
	                    }
	                }
	                else
	                {
	                    //printf("In else \n");
	                    DecodedData[l]=ReceivedDataPointer[j];
	                    DecodedData[l+1]=ReceivedDataPointer[j+1];
	                    DecodedData[l+2]=ReceivedDataPointer[j+2];
	                    DecodedData[l+3]=ReceivedDataPointer[j+3];
	                    DecodedData[l+4]=ReceivedDataPointer[j+4];
	                    DecodedData[l+5]=ReceivedDataPointer[j+5];
	                    DecodedData[l+6]=ReceivedDataPointer[j+6];
	                    DecodedData[l+7]=ReceivedDataPointer[j+7];
	                }
	                i+=4;// syndrom matrix counter
	                j+=12; //received data counter
	                l+=8; //decoded data counter
	            }
	            //printf("\n");

	            //                for(i=0;i<256;i++)
	            //                {
	            //                    printf("%d",DecodedData[i]);
	            //                }printf("\n");

}
void lbc_decode2(int generator_typ,int x,int y,int data_length,bool *buff,bool *DecodedData)
{
	int i=0,j=0;
	bool H2[14][6]={{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0}};
	bool Gen2[8][14] ={G21,G22,G23,G24,G25,G26,G27,G28};
	//Generate Parity Check matrix using generator matrix
	            for (i=0;i<8;i++)
	            {
	                for(j=0;j<6;j++)
	                {
	                    H2[i][j] = Gen2[i][j+8];
	                }
	            }
	            for (j=0;j<6;j++)
	            {
	                H2[8+j][j]=1;
	            }
	                 /* printf("\nParity Matrix:\n");
	                  for (i=0;i<14;i++)
	                  {
	                      for(j=0;j<6;j++)
	                          printf("%d ",H2[i][j]);
	                      printf("\n");
	                  }*/
	                  int InpSize;

	                  InpSize=data_length*14;
	                  //Generate Syndrome Matrix
	                bool Syndrome[data_length*6];
	                bool *SyndromeDataPointer = Syndrome;
	                bool *ReceivedDataPointer = buff+((x*8)+y+24);
	                int counter=0;
	                int InpCounter=(x*8)+y+24;
	                while(counter < data_length*6)
	                {
	                    for (i=0; i<6; i++)
	                    {
	                        Syndrome[counter] = 0;
	                        for (j=0;j<14;j++)
	                        {
	                            Syndrome[counter]=Syndrome[counter]^(buff[InpCounter+j]*H2[j][i]);
	                            //printf("%d ",CodedText[counter]);
	                        }
	                        counter++;
	                        //printf("\n");
	                    }
	                    InpCounter += 14;
	                }

/*	                i=0;
	                printf("Syndrome matrix:\n");
	                while(i<data_length*6)
	                {
	                    for(j=0;j<6;j++)
	                    {
	                        printf("%d ",Syndrome[i]);
	                        i++;
	                    }
	                    printf("\n");
	                }*/


	                i=0;j=0;
	                int k=0,l=0;
	                int Distance;
	                x=0;



	                while(i<data_length*6)
	                {
	                    // printf("x = %d\n",x);
	                    x = (SyndromeDataPointer[i]^0)+(SyndromeDataPointer[i+1]^0)+(SyndromeDataPointer[i+2]^0)+(SyndromeDataPointer[i+3]^0)+(SyndromeDataPointer[i+4]^0)+(SyndromeDataPointer[i+5]^0);
	                    //  printf("x = %d\n",x);
	                    ///printf("i = %d\n",i);
	                    //printf("j = %d\n",j);
	                    //printf("l = %d\n",l);

	                    k=0;
	                    if (x!=0)
	                    {
	                        DecodedData[l]=ReceivedDataPointer[j];
	                        DecodedData[l+1]=ReceivedDataPointer[j+1];
	                        DecodedData[l+2]=ReceivedDataPointer[j+2];
	                        DecodedData[l+3]=ReceivedDataPointer[j+3];
	                        DecodedData[l+4]=ReceivedDataPointer[j+4];
	                        DecodedData[l+5]=ReceivedDataPointer[j+5];
	                        DecodedData[l+6]=ReceivedDataPointer[j+6];
	                        DecodedData[l+7]=ReceivedDataPointer[j+7];

	                        while(k<1330) //1330 is length of coded ascii array
	                        {
	                            Distance= (CodedASCIITable2[k]^ReceivedDataPointer[j])+
	                                    (CodedASCIITable2[k+1]^ReceivedDataPointer[j+1])+
	                                    (CodedASCIITable2[k+2]^ReceivedDataPointer[j+2])+
	                                    (CodedASCIITable2[k+3]^ReceivedDataPointer[j+3])+
	                                    (CodedASCIITable2[k+4]^ReceivedDataPointer[j+4])+
	                                    (CodedASCIITable2[k+5]^ReceivedDataPointer[j+5])+
	                                    (CodedASCIITable2[k+6]^ReceivedDataPointer[j+6])+
	                                    (CodedASCIITable2[k+7]^ReceivedDataPointer[j+7])+
	                                    (CodedASCIITable2[k+8]^ReceivedDataPointer[j+8])+
	                                    (CodedASCIITable2[k+9]^ReceivedDataPointer[j+9])+
	                                    (CodedASCIITable2[k+10]^ReceivedDataPointer[j+10])+
	                                    (CodedASCIITable2[k+11]^ReceivedDataPointer[j+11])+
	                                    (CodedASCIITable2[k+12]^ReceivedDataPointer[j+12])+
	                                    (CodedASCIITable2[k+13]^ReceivedDataPointer[j+13]);
	                            //printf("Distance: %d\n",Distance);
	                            if (Distance == 1)
	                            {
	                                DecodedData[l]=CodedASCIITable2[k];
	                                DecodedData[l+1]=CodedASCIITable2[k+1];
	                                DecodedData[l+2]=CodedASCIITable2[k+2];
	                                DecodedData[l+3]=CodedASCIITable2[k+3];
	                                DecodedData[l+4]=CodedASCIITable2[k+4];
	                                DecodedData[l+5]=CodedASCIITable2[k+5];
	                                DecodedData[l+6]=CodedASCIITable2[k+6];
	                                DecodedData[l+7]=CodedASCIITable2[k+7];
	                                break;
	                            }
	                            //printf("k = %d\n",k);
	                            k+=14; //coded ascii table counter
	                        }
	                    }
	                    else
	                    {
	                        //printf("In else \n");
	                        DecodedData[l]=ReceivedDataPointer[j];
	                        DecodedData[l+1]=ReceivedDataPointer[j+1];
	                        DecodedData[l+2]=ReceivedDataPointer[j+2];
	                        DecodedData[l+3]=ReceivedDataPointer[j+3];
	                        DecodedData[l+4]=ReceivedDataPointer[j+4];
	                        DecodedData[l+5]=ReceivedDataPointer[j+5];
	                        DecodedData[l+6]=ReceivedDataPointer[j+6];
	                        DecodedData[l+7]=ReceivedDataPointer[j+7];
	                    }
	                    i+=6;// syndrom matrix counter
	                    j+=14; //received data counter
	                    l+=8; //decoded data counter
	                }
}
#endif /* LISA_H_ */
